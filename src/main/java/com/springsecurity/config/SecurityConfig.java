package com.springsecurity.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.provisioning.InMemoryUserDetailsManager;

@Configuration
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(AuthenticationManagerBuilder auth) throws Exception {

        auth.inMemoryAuthentication().withUser("admin").password("{noop}admin").authorities("ADMIN");
        auth.inMemoryAuthentication().withUser("employee").password("{noop}employee").authorities("EMPLOYEE");

    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().antMatchers("/").permitAll()
                .antMatchers("/home").authenticated()
                .antMatchers("/edashboard").hasAuthority("EMPLOYEE")
                .antMatchers("/adashboard").hasAuthority("ADMIN")
                .anyRequest().authenticated()
                .and().formLogin();
        //.and().exceptionHandling().accessDeniedPage("/access-denied");

    }
}
