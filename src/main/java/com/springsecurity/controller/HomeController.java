package com.springsecurity.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class HomeController {

    @GetMapping("/home")
    public String home() {
        return "home";
    }
    @GetMapping("/")
    public String login() {
        return "index";
    }
    @GetMapping("/adashboard")
    public String dashboard() {
        return "admin-dashboard";
    }
    @GetMapping("/edashboard")
    public String edashboard() {
        return "employee-dashboard";
    }
    @GetMapping("/access-denied")
    public String accessDenied() {
        return "access-denied";
    }

}
